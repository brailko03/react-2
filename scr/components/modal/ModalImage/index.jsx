const src = "/productImages/1.jpg";
const ModalImage = () => {
    
    return (<div className="modalImage">
        {<img src={src} alt="product" />}
    </div>)
}

export default ModalImage;
